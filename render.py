from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jnpr.junos.exception import *
from jinja2 import Template
import yaml
import sys
import os

if not os.path.isdir('configs'):
    os.mkdir('configs')

for site in os.listdir('.'):
    if os.path.isfile(site) or site == 'templates' or site.startswith('.') or site == 'configs':
        continue
    print('rendering ' + site + ' ...')

    confpath = 'configs/' + site
    if not os.path.isdir(confpath):
        os.mkdir(confpath)

    for devicefile in os.listdir(site):
        if not devicefile.endswith('.yml') or devicefile.startswith('.'):
            continue

        devicetype = devicefile[:-4]
        print(devicefile)

        fv = open(site+'/'+devicefile)
        data = fv.read()
        my_vars = yaml.load(data, Loader=yaml.SafeLoader)
        fv.close()

        fj = open('templates/'+devicetype+'.j2')
        j2 = fj.read()
        template = Template(j2)
        fj.close()

        print('building configuration files ...')
        for device in my_vars:
            conffilename = confpath+'/'+device["host_name"]+'.conf.txt'
            print('generate config file '+conffilename+' ...')
            conffile = open(conffilename, 'w')
            conffile.write(template.render(device))
            conffile.close()

    print('')

print('done')
