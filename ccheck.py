from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jnpr.junos.exception import *
from jinja2 import Template
import yaml
import sys
import os
import argparse

import warnings
# ignore CryptographyDeprecationWarning
warnings.filterwarnings(action='ignore', module='.*paramiko.*')

parser = argparse.ArgumentParser()
parser.add_argument('--site', help='DC site name')
parser.add_argument('--user', help='device login username')
args = parser.parse_args()

if args.site == None:
    print('please specify site name using --site <name> argument')
    exit(1)

if args.user == None:
    print('please specify device login name using --user <name> argument')
    exit(1)

print('\ncommit check validation for site ' + args.site + ' ...\n')

confpath = 'configs/' + args.site
if not os.path.isdir(confpath):
    print('no configs folder ' + confpath + ' found')
    exit(1)

for devicefile in os.listdir(args.site):
    if not devicefile.endswith('.yml') or devicefile.startswith('.'):
        continue

    devicetype = devicefile[:-4]

    fv = open(args.site+'/'+devicefile)
    data = fv.read()
    my_vars = yaml.load(data, Loader=yaml.SafeLoader)
    fv.close()

    for device in my_vars:
        conffilename = confpath+'/'+device["host_name"]+'.conf.txt'
        print('checking config file '+conffilename +
              ' on device ' + device["host_name"] + ' mgmt ip ' + device['management_ip'] + ' ...')
        dev = Device(host=device["management_ip"], user=args.user)
        dev.open()
        cfg = Config(dev)
        cfg.rollback()  # Execute Rollback to prevent commit change from old config session
        cfg.load(path=conffilename, format='text')
        if cfg.commit_check() == True:
            print ('configuration via commit_check validated on ' +
                   dev.facts["hostname"])
        else:
            print ('commit_check failed on ' + dev.facts["hostname"])
        dev.close()

print('done')
